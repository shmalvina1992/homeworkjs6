"use strict";

// 1. Чтоб найти или использовать спец. символ как обычный, нужно добавить к нему "\", тобишь экранировать его,
// дать понять браузеру.
// 2. Function Declaration, Function Expression, Named Function Expression.
// 3. Hosting - это механизм, в котором переменные и объявленные функций, передвигаются вверх своей области видимости перед тем, как код будет выполнен. 
// Это означает то, что неважно где были объявлены функция или переменные, все они передвигаются вверх своей области видимости, не зависимо
// от того локальная область или глобальная.

const firstNameValue = prompt ('Please, write your first name!');
const lastNameValue = prompt ('Please, write your last name!');
const birthdayValue = new Date (prompt ('Please, write date of birth!', 'dd.mm.yyyy').split('.').reverse());

function createNewUser() {
    const newUser = {
        _firstName: firstNameValue,
        _lastName: lastNameValue,
        birthday: birthdayValue,
        birthdayForPassword: birthdayValue.getFullYear(),
        getAge() {
            let now = new Date();
            let getNowDate = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            let birthDate = this.birthday;
            let dayOfBirthNow = new Date(getNowDate.getFullYear(), birthDate.getMonth(), birthDate.getDate());
            let result = getNowDate.getFullYear() - birthDate.getFullYear();
            if (getNowDate < dayOfBirthNow) {
                result -= 1;
            }
            
            return result;
        },
        getPassword() {
            return `${this._firstName[0]}`.toUpperCase() + `${this._lastName}`.toLowerCase() + `${this.birthdayForPassword}`; 
        }, 
        getLogin() {
            return `${this._firstName[0]}${this._lastName}`.toLowerCase();
        },
        setFirstName (value) {
            this._firstName = value;
        },
        get firstName () {
            return this._firstName;
        },
        setLastName (value) {
            this._lastName = value;
        },
        get lastName () {
            return this._lastName;
        }
    }
    
    return newUser;
}

const oneUser = createNewUser();
console.log(oneUser.getAge());
console.log(oneUser.getPassword());